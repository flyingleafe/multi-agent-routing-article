with import <nixpkgs> {}; {
  thesisTextEnv = stdenv.mkDerivation {
    name = "thesis-text-env";
    buildInputs = [
      (texlive.combine {
        inherit (texlive) 
          scheme-small
          todonotes
          stmaryrd
          biber
          collection-langcyrillic
          csquotes
	  dblfloatfix
          etoolbox
          elsarticle
	  float
          tabu
          multirow
          varwidth
          floatrow
          algorithms
          algorithmicx
          enumitem
          biblatex
          biblatex-gost
          logreq
          xstring
          lastpage
          totcount
          chngcntr
          titlesec
          paratype
          was
          filecontents
          cm-super
          subfigure
          appendixnumberbeamer
	  siunitx
          symbol;
      })
      pdftk
      ghostscript
      biber
      graphviz
    ];
  };
}
