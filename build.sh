#!/bin/bash

pdflatex article
#biber    article
bibtex article
pdflatex article
pdflatex article

rm article.{bib,aux,log,bbl,bcf,blg,run.xml,toc,tct,spl}
